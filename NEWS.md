# g2.s.gwl 1.0.0

* Added the last function that calculate the Linkage for all phenotypes in parallel
* Added the option to make sex-specific linkage

# g2.s.gwl 0.2.0

* Added the requires(packages) necessary for the package to function
* Allow generic correction in the map for non-monotonous function
* Passed th lifecycle to `stable`

# g2.s.gwl 0.1.0

* Added the license file
* Added more dependencies
* Now we store the dictionary map to translate from *bp* to *cM* and to fictional *cM*
* Solve the problem with non-monotonous function of cM, when merge out-range(our map) with 
in-range(the reference map)

# g2.s.gwl 0.0.2

* Now is the package download and compile the `IdsCoefs` and the `ibdld` programs
* Now only calculate the ibd matrix for each *cM* not for all *SNPs*
* Change in the `IdsCoefs` directory
* Corrected the problem when liftover jump some *SNPs* to other chromosomes
* Corrected the problem the names of the *IBDs* files, previous names aren't recognize for `solarius`
* Added the resolution parameter in the creation of *mIBDs* point files. This allow us, to estimate
multiple *mIBDs* per *cM*

# g2.s.gwl 0.0.1

* Added a `NEWS.md` file to track changes to the package
* Added the dependencies to the package
* Change in closing the gds file function
