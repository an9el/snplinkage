
##' Join all the files with the genotypes with NAs in the position of mendelian errors
##'
##' Just the genotype slot has been modified
##' @title Create a gds file without mendelian errors
##' @param dirs list created with the function g2.s.gwl.0.createDirs
##' @param Nparts integer, the number of parts in which we split the samples to lower RAM usage
##' @return A gds file with the NAs in the mendelian errors positions. The file is a copy of the original file,
##' except for the NAs in the genotype slot
##' @author person("Angel", "Martinez-Perez",
##' email = "angelmartinez@protonmail.com",
##' role = c("aut", "cre"),
##' comment = c(ORCID = "0000-0002-5934-1454"))
##' @note 2020-05-28.
##' @examples
##' \dontrun{
##' g2.s.gwl.6.joinGenotypes(dirs, Nparts = 20L)}
##' 
##' @export
g2.s.gwl.6.joinGenotypes <- function(dirs, Nparts = 20L) {

    checkmate::assertList(dirs, len = 12, types = c("logical", "character", "integer"))
    checkmate::assertInteger(Nparts, lower = 1, upper = 100)
    ## TODO, check that Nparts match the number of files
    genoFile <- as.character(glue::glue("{dirs$gds.GWASTools}/chr{dirs$chr}.gds"))
    checkmate::assertFileExists(genoFile, access = "r")
    genoFileNoErrors <- as.character(glue::glue("{dirs$gds.noMendel}/chr{dirs$chr}.gds"))
    file.copy(from = genoFile, to = genoFileNoErrors, overwrite = FALSE)
    etiqueta <- glue::glue("c{1:Nparts}")
    
    for (i in 1:Nparts) {
        fileInyectar <- glue::glue("{dirs$data.mendel}/gt.nomendel.chr{dirs$chr}.{etiqueta[i]}.inyectar.RData")
        checkmate::assertFileExists(fileInyectar, access = "r")
        load(fileInyectar)
        rm(fileInyectar)
        
        ## For all genotypes...
        gds <- GdsGenotypeReader(genoFileNoErrors)
        geno <- getVariable(gds, "genotype")
        showfile.gds(closeall = TRUE)    
        ## Just put NAs in the positions where this file indicate
        geno[posicionesEnGrande, ] <- genoSub
        rm(genoSub)

        gds <- openfn.gds(genoFileNoErrors, readonly = FALSE)
        ## Change the genotypes for the new ones with the NAs
        add.gdsn(node = gds,
                 name = "genotype",
                 val = geno,
                 replace = TRUE,
                 check = TRUE,
                 compress = "LZMA_RA",
                 storage = "packedreal16",
                 closezip = TRUE,
                 valdim = c(nrow(geno), ncol(geno)))
        rm(geno)
        showfile.gds(closeall = TRUE)
    }
    return(invisible())
}










