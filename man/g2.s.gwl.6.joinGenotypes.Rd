% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/g2.s.gwl.6.joinGenotypes.R
\name{g2.s.gwl.6.joinGenotypes}
\alias{g2.s.gwl.6.joinGenotypes}
\title{Create a gds file without mendelian errors}
\usage{
g2.s.gwl.6.joinGenotypes(dirs, Nparts = 20L)
}
\arguments{
\item{dirs}{list created with the function g2.s.gwl.0.createDirs}

\item{Nparts}{integer, the number of parts in which we split the samples to lower RAM usage}
}
\value{
A gds file with the NAs in the mendelian errors positions. The file is a copy of the original file,
except for the NAs in the genotype slot
}
\description{
Join all the files with the genotypes with NAs in the position of mendelian errors
}
\details{
Just the genotype slot has been modified
}
\note{
2020-05-28.
}
\examples{
\dontrun{
g2.s.gwl.6.joinGenotypes(dirs, Nparts = 20L)}

}
\author{
person("Angel", "Martinez-Perez",
email = "angelmartinez@protonmail.com",
role = c("aut", "cre"),
comment = c(ORCID = "0000-0002-5934-1454"))
}
