Readme of the g2.s.gwl package
================
Angel Martinez-Perez
(2020-10-09)

<!-- README.md is generated from README.Rmd. Please edit that file -->

-----

# g2.s.gwl

<!-- badges: start -->

[![Lifecycle:
stable](https://img.shields.io/badge/lifecycle-stable-brightgreen.svg)](https://www.tidyverse.org/lifecycle/#stable)
<!-- badges: end -->

The goal of g2.s.gwl is to proccess the genetic data stored in a gds
file, to be able to run a linkage analysis.

This package is really oriented to work in GAIT2 data with `gait2`
package, so probably does not fit your data well. You can still use it
as a guide.

## Installation

You can install the released version of *g2.s.gwl* package from
[gitlab](https://gitlab.com) with:

``` r
remotes::install_gitlab("an9el/snplinkage")
```

## Workflow

Basically this package is a list of functions that run sequentially

This is the normal workflow which shows you how to use this package:

``` r
## load the required libraries
suppressPackageStartupMessages({ 
    library(pacman)
    p_load(gait2, GWASTools, tidyverse, g2.s.gwl, magrittr, glue, checkmate, tidyr, rtracklayer,
    SeqVarTools, solarius,  mlr,  details, data.table)
})  
## basic run code
dirs <- g2.s.gwl.0.createDirs(chr = 22L , external = FALSE, createDirs = TRUE, downloadAuxFiles = TRUE)
g2.s.gwl.1.gds2GWASTools(dirs)
g2.s.gwl.2.lift_over(dirs)
g2.s.gwl.3.mendelErrors(dirs)
g2.s.gwl.4.injectNAs(dirs, Nparts = 20L)
g2.s.gwl.5.injectNAs.out(dirs, Nparts = 20L)
g2.s.gwl.6.joinGenotypes(dirs, Nparts = 20L)
filters <- g2.s.gwl.7.filters(N = 934L)

cMmap <- g2.s.gwl.8.bp2cM(dirs, filters, plot = TRUE)
g2.s.gwl.9.ibdlPrep(dirs, filters, cMmap, resolution = 10L)
g2.s.gwl.10.idcoefs(dirs)
g2.s.gwl.11.ibdld(dirs, nthreads = 60L)
g2.s.gwl.12.ibdld2solarius(dirs)
g2.s.gwl.14.RUNall()
```

# Explanation

What each function does:

### g2.s.gwl.0.createDirs

Create a list with some parameters and the folder structure for the use
of the rest of the files. Note that we process only one chromosome at a
time.  
Also, if required, it download 2 files to run the analysis.  
You can see more information
[Here](https://www.bioconductor.org/packages/devel/workflows/vignettes/liftOver/inst/doc/liftov.html)

### g2.s.gwl.1.gds2GWASTools

This function just translate the gds file into the GWASTools format (the
initial format was in SeqGDS format which is not utter compatible with
the functions in GWASToos)

### g2.s.gwl.2.lift\_over

This function lift\_over the position map from **hg19** to **hg38**.  
More information
[Here](https://www.bioconductor.org/packages/devel/workflows/vignettes/liftOver/inst/doc/liftov.html)

### g2.s.gwl.3.mendelErrors

Check for Mendelian errors into the the most likely genotype

### g2.s.gwl.4.injectNAs

We replace the likely Mendelian errors by `NA` in this position-subject

### g2.s.gwl.5.injectNAs.out

This function cut the gds file in GWASTools format into pieces.  
For each piece, it stores the positions in the original file to allow
assemble again into it. Each piece contain the missing genotypes
(missing genotypes inserted for being detected as Mendelian errors).

### g2.s.gwl.6.joinGenotypes

This function made a copy of the gds file in GWASTools format and insert
the missing genotypes (genotypes that have Mendelian errors), into it.
This means, this function, assemble the different pieces produced in the
previous function into one.

### g2.s.gwl.7.filters

This function create some filter that I use to exclude some variants.
Now the defaults are fixed and if you want to modify it you need to
create your own function. This is made on purpose. This parameters are
specific for this study.

### g2.s.gwl.8.bp2cM

With this function you are able to translate your map in *bp* into a map
in *cM* that will be used in the proper linkage analysis. It has some
tricky parts:

1.  The reference map do not cover all the range of the actual
    chromosome, so we need to extrapolate the predictions of the point
    outside this range. We made that using a spline regression (with the
    earth package).  
2.  Then with all the range cover by our data, we made a fast k-Nearest
    Neighbor to interpolate our data in cM.  
3.  Finally we move all the map towards right to assure all the values
    are positives.

### g2.s.gwl.9.ibdlPrep

This function create the files used by the program `ibdld`.

Genotypes are also transformed into the format used by this program.

| file genotype | ibdld genotype |
| :-----------: | :------------: |
|      1/1      |      2/2       |
|      1/0      |      2/1       |
|      0/1      |      1/2       |
|      0/0      |      1/1       |
|      NA       |      0/0       |

formatChange

This function also have an interesting integer parameter `resolution`.
For example, if this parameter = 10, then 10 *mIBD* matrices a maximum
of 10 matrices wil be created for each *cM*. An index of fictitious
*cM*, starting from `0`, will be created for solarius to manage it. At
the end of the process you have to undo the process. For this task you
need the *cMmap* in the file `chrXX.prefix.SNP.RData`.  
This file contain the real `bp` coordinates, the real `cM` coordinates
and the `fictitious cM` coordinates (*cMent*).

### g2.s.gwl.10.idcoefs

This function launch the idCoefs function

### g2.s.gwl.11.ibdld

Just launch ibdld. Keep in mind that if your `resolution` parameter in
the function `g2.s.gwl.9.ibdlPrep` is greater than `1` then your output
*cM* will be `fake` in the name of the files mibd.chr.cM.gz . Then in
the function `g2.s.gwl.13.solarius.R`, it detects if the resolution was
used and undone the change.

### g2.s.gwl.12.ibdld2solarius

This function takes the output of ibdld, that came into one big matrix
and cut into multiple ibd files (one per locus).  
The output are files like:

| id1 | id2 | matrix1 | matrix2 |
| :-: | :-: | :-----: | :-----: |
| 11  | 11  |    1    |    1    |
| 12  | 12  |    1    |    1    |
| 13  | 11  |   0.5   |    0    |
| 14  | 13  |  0.931  |  0.25   |

output

**VERY IMPORTANT** the output of this function must be files with the
exact nomenclature:

> `mibd.chr.cM.gz`

For example, in the chromosome 2, centiMorgans 0,1,2, the mibd will
named:

> mibd.2.0.gz  
> mibd.2.1.gz  
> mibd.2.2.gz

### g2.s.gwl.13.solarius

This function just run the `solarius` package to launch the linkage
analysis with the phenotype of interest with the function
`solarMultipoint`.

If the `resolution` parameter is used then the map of the output is
corrected to reflect the real `cM` not the fakes. You can select if your
phenotype must be normalized or not.

Also you can pass several parameters to the `solarMultipoint` function,
like:

1.  **cores** to use multiple cores
2.  **multipoint.options = “3”** if you want to calculate a second pass
    of linkage analysis conditioning in the biggest LOD score
3.  **interval** a X integer to only calculate only one each X IBDs
4.  **chr** to calculate only linkage in that chromosome
5.  **multipoint.settings = “finemap off”** to disable finemap in
    `SOLAR`
6.  See this
    [link](https://ugcd.github.io/solarius/doc/solarMultipoint.html) for
    more information about this and more options

This function is called within the next function `g2.s.gwl.14.RUNall()`
which calculate all the GWL at the same time. This function is just for
calculate only one GWL.

### g2.s.gwl.14.RUNall

This function assume that the `IBDs` matrices are in the folder given by
the `gait2` package, it puts the results (the models and the plots) in
the `outDir` folder. `mcapply` use cores \> 1 only in Linux, so if you
are using Windows you have to change that to cores = 1L. Added also
sex-specific linkage option.

# License

> Copyright (C) 2020 Angel Martinez-Perez</br>
> 
> This program is free software: you can redistribute it and/or modify  
> it under the terms of the GNU General Public License as published by  
> the Free Software Foundation, either version 3 of the License, or  
> (at your option) any later version.</br>
> 
> This program is distributed in the hope that it will be useful,  
> but WITHOUT ANY WARRANTY; without even the implied warranty of  
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the  
> GNU General Public License for more details.</br>
> 
> You should have received a copy of the GNU General Public License  
> along with this program. If not, see <http://www.gnu.org/licenses>.

<details closed>

<summary> <span title="Clik to Expand"> Current session info </span>
</summary>

``` r

─ Session info ───────────────────────────────────────────────────────────────
 setting  value                       
 version  R version 4.0.2 (2020-06-22)
 os       Debian GNU/Linux 10 (buster)
 system   x86_64, linux-gnu           
 ui       X11                         
 language (EN)                        
 collate  en_US.UTF-8                 
 ctype    en_US.UTF-8                 
 tz       Europe/Madrid               
 date     2020-10-09                  

─ Packages ───────────────────────────────────────────────────────────────────
 package       * version  date       lib source        
 assertthat      0.2.1    2019-03-21 [2] CRAN (R 4.0.1)
 backports       1.1.10   2020-09-15 [2] CRAN (R 4.0.2)
 bibtex          0.4.2.3  2020-09-19 [2] CRAN (R 4.0.2)
 blob            1.2.1    2020-01-20 [2] CRAN (R 4.0.1)
 broom           0.7.1    2020-10-02 [2] CRAN (R 4.0.2)
 callr           3.4.4    2020-09-07 [2] CRAN (R 4.0.2)
 cellranger      1.1.0    2016-07-27 [2] CRAN (R 4.0.1)
 cli             2.0.2    2020-02-28 [2] CRAN (R 4.0.1)
 colorspace      1.4-1    2019-03-18 [2] CRAN (R 4.0.1)
 crayon          1.3.4    2017-09-16 [2] CRAN (R 4.0.1)
 DBI             1.1.0    2019-12-15 [2] CRAN (R 4.0.1)
 dbplyr          1.4.4    2020-05-27 [2] CRAN (R 4.0.1)
 desc            1.2.0    2018-05-01 [2] CRAN (R 4.0.1)
 devtools      * 2.3.2    2020-09-18 [2] CRAN (R 4.0.2)
 digest          0.6.25   2020-02-23 [2] CRAN (R 4.0.1)
 dplyr         * 1.0.2    2020-08-18 [2] CRAN (R 4.0.2)
 ellipsis        0.3.1    2020-05-15 [2] CRAN (R 4.0.1)
 evaluate        0.14     2019-05-28 [2] CRAN (R 4.0.1)
 fansi           0.4.1    2020-01-08 [2] CRAN (R 4.0.1)
 filehash        2.4-2    2019-04-17 [2] CRAN (R 4.0.1)
 forcats       * 0.5.0    2020-03-01 [2] CRAN (R 4.0.1)
 fs              1.5.0    2020-07-31 [2] CRAN (R 4.0.2)
 generics        0.0.2    2018-11-29 [2] CRAN (R 4.0.1)
 ggplot2       * 3.3.2    2020-06-19 [2] CRAN (R 4.0.1)
 glue            1.4.2    2020-08-27 [2] CRAN (R 4.0.2)
 gtable          0.3.0    2019-03-25 [2] CRAN (R 4.0.1)
 haven           2.3.1    2020-06-01 [2] CRAN (R 4.0.1)
 hms             0.5.3    2020-01-08 [2] CRAN (R 4.0.1)
 htmltools       0.5.0    2020-06-16 [2] CRAN (R 4.0.1)
 httr            1.4.2    2020-07-20 [2] CRAN (R 4.0.2)
 jsonlite        1.7.1    2020-09-07 [2] CRAN (R 4.0.2)
 knitcitations * 1.0.10   2019-09-15 [2] CRAN (R 4.0.1)
 knitr         * 1.30     2020-09-22 [2] CRAN (R 4.0.2)
 lifecycle       0.2.0    2020-03-06 [2] CRAN (R 4.0.1)
 lubridate       1.7.9    2020-06-08 [2] CRAN (R 4.0.1)
 magrittr      * 1.5      2014-11-22 [2] CRAN (R 4.0.1)
 memoise         1.1.0    2017-04-21 [2] CRAN (R 4.0.1)
 modelr          0.1.8    2020-05-19 [2] CRAN (R 4.0.1)
 munsell         0.5.0    2018-06-12 [2] CRAN (R 4.0.1)
 pacman        * 0.5.1    2019-03-11 [2] CRAN (R 4.0.1)
 pillar          1.4.6    2020-07-10 [2] CRAN (R 4.0.2)
 pkgbuild        1.1.0    2020-07-13 [2] CRAN (R 4.0.2)
 pkgconfig       2.0.3    2019-09-22 [2] CRAN (R 4.0.1)
 pkgload         1.1.0    2020-05-29 [2] CRAN (R 4.0.1)
 plyr            1.8.6    2020-03-03 [2] CRAN (R 4.0.1)
 prettyunits     1.1.1    2020-01-24 [2] CRAN (R 4.0.1)
 processx        3.4.4    2020-09-03 [2] CRAN (R 4.0.2)
 ps              1.3.4    2020-08-11 [2] CRAN (R 4.0.2)
 purrr         * 0.3.4    2020-04-17 [2] CRAN (R 4.0.1)
 R6              2.4.1    2019-11-12 [2] CRAN (R 4.0.1)
 Rcpp            1.0.5    2020-07-06 [2] CRAN (R 4.0.1)
 readr         * 1.4.0    2020-10-05 [2] CRAN (R 4.0.2)
 readxl          1.3.1    2019-03-13 [2] CRAN (R 4.0.1)
 RefManageR    * 1.2.12   2019-04-03 [2] CRAN (R 4.0.1)
 remotes         2.2.0    2020-07-21 [2] CRAN (R 4.0.2)
 reprex          0.3.0    2019-05-16 [2] CRAN (R 4.0.1)
 rlang           0.4.7    2020-07-09 [2] CRAN (R 4.0.1)
 rmarkdown     * 2.4      2020-09-30 [2] CRAN (R 4.0.2)
 roxygen2      * 7.1.1    2020-06-27 [2] CRAN (R 4.0.1)
 rprojroot       1.3-2    2018-01-03 [2] CRAN (R 4.0.1)
 rstudioapi      0.11     2020-02-07 [2] CRAN (R 4.0.1)
 rvest           0.3.6    2020-07-25 [2] CRAN (R 4.0.2)
 scales          1.1.1    2020-05-11 [2] CRAN (R 4.0.1)
 sessioninfo     1.1.1    2018-11-05 [2] CRAN (R 4.0.1)
 stringi         1.5.3    2020-09-09 [2] CRAN (R 4.0.2)
 stringr       * 1.4.0    2019-02-10 [2] CRAN (R 4.0.1)
 testthat      * 2.3.2    2020-03-02 [2] CRAN (R 4.0.1)
 tibble        * 3.0.3    2020-07-10 [2] CRAN (R 4.0.2)
 tidyr         * 1.1.2    2020-08-27 [2] CRAN (R 4.0.2)
 tidyselect      1.1.0    2020-05-11 [2] CRAN (R 4.0.1)
 tidyverse     * 1.3.0    2019-11-21 [2] CRAN (R 4.0.1)
 tikzDevice    * 0.12.3.1 2020-06-30 [2] CRAN (R 4.0.1)
 usethis       * 1.6.3    2020-09-17 [2] CRAN (R 4.0.2)
 vctrs           0.3.4    2020-08-29 [2] CRAN (R 4.0.2)
 withr           2.3.0    2020-09-22 [2] CRAN (R 4.0.2)
 xfun            0.18     2020-09-29 [2] CRAN (R 4.0.2)
 xml2            1.3.2    2020-04-23 [2] CRAN (R 4.0.1)
 yaml            2.2.1    2020-02-01 [2] CRAN (R 4.0.1)

[1] /home/amartinezp/R/x86_64-pc-linux-gnu-library/4.0
[2] /usr/local/lib/R/site-library
[3] /usr/lib/R/site-library
[4] /usr/lib/R/library
```

</details>

<br>
